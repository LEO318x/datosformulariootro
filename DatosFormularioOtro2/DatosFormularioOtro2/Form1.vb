﻿Public Class Form1
    'Variable privada solo accesible desde el mismo formulario
    Private variableprivada As String = "Privado"
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim formulariodato As New Form2()
        formulariodato.Show()
    End Sub

    'Metodo publico que me servira para modificar los datos del formulario incluso si estan "privados"
    Public Sub modificarT(texto As String)
        variableprivada = texto
        TextBox1.Text = variableprivada
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TextBox1.Text = variableprivada
        TextBox1.ReadOnly = True
    End Sub
End Class
